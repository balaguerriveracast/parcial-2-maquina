//Crear una aplicacion que simule una mquina dispensadora de alimentos.
//Debe tener un maximo de 10 productos y solo debe aceptar monedas de (5,10,25) y billetes de (50,100 y 200)

using System;

class MainClass 
{
  public class Producto
  {
    public int precio=0;
    public int cola=0;
    public int colasina=0;
    public int pepsi=0;
    public int kola=0;
    public int melocoton=0;
    public int pera=0;
    public int frutpunch=0;
    public int india=0;
    public int morena=0;
    public int cafe=0;

    public void Mostrar()
    {
      int op;
      do
      {
        Console.WriteLine(" ");
        Console.WriteLine("Maquina X");
        Console.WriteLine("1. Ingresar moneda");
        Console.WriteLine("2. Elije un producto");
        Console.WriteLine("3. Salir");
        Console.Write("Ingrese una opcion:");
        op= int.Parse(Console.ReadLine());
        Console.WriteLine(" ");
        if (op==1)
        {
          Console.WriteLine("1. Incertar monedas de 5.");
          Console.WriteLine("2. Incertar monedas de 10.");
          Console.WriteLine("3. Incertar monedas de 25.");
          Console.WriteLine("4. Incertar billetes de 50.");
          Console.WriteLine("5. Incertar billetes de 100.");
          Console.WriteLine("6. Incertar billetes de 100.");
          Console.Write("Elija una opcion: ");
          int op2= int.Parse(Console.ReadLine());
          if (op2==1)
          {
            Console.Write("incerte: ");
            precio= int.Parse(Console.ReadLine());

            precio*=5;
            Console.WriteLine("Total de monedas de 5: " +precio);

          }
          if (op2==2)
          {
            Console.Write("incerte: ");
            precio= int.Parse(Console.ReadLine());

            precio*=10;
            Console.WriteLine("Total de monedas de 10: " +precio);

          }
          if (op2==3)
          {
            Console.Write("incerte: ");
            precio= int.Parse(Console.ReadLine());

            precio*=25;
            Console.Write("Total de monedas de 25: " +precio);

          }
          if (op2==4)
          {
            Console.WriteLine("incerte: ");
            precio= int.Parse(Console.ReadLine());

            precio*=50;
            Console.Write("Total de billetes de 50: " +precio);
          }
          if (op2==5)
          {
            Console.Write("incerte: ");
            precio= int.Parse(Console.ReadLine());

            precio*=100;
            Console.WriteLine("Total de billetes de 100: " +precio);
          }
          if (op2==6)
          {
            Console.Write("incerte: ");
            precio= int.Parse(Console.ReadLine());

            precio*=200;
            Console.WriteLine("Total de billetes de 200: " +precio);
          }
          if (op2<=0 || op2>=7)
          {
            Console.WriteLine("Invalido");
          }
        }
        if (op==2)
        {
          Console.WriteLine("Productos");
          Console.WriteLine("1. Coca-Cola RD$25");
          Console.WriteLine("2. Coca-Coca sin azucar RD$25");
          Console.WriteLine("3. Pepsi RD$25");
          Console.WriteLine("4. Kola Real sabor cola RD$10");
          Console.WriteLine("5. Santal Melocoton RD$15");
          Console.WriteLine("6. Santal Pera RD$15");
          Console.WriteLine("7. Santal Frutpunch RD$15");
          Console.WriteLine("8. Malta India RD$25");
          Console.WriteLine("9. Malta Morena RD$25");
          Console.WriteLine("10. Cafe RD$10");
          Console.Write("Elije un producto: ");
          int m= int.Parse(Console.ReadLine());
          if (m==1)
          {
            if (precio>=25)
            {
              Console.WriteLine("Has Comprado una Coca-Cola");
              precio-=25;
              cola+=1;
              Console.WriteLine("Te quedan RD$ {0}" ,precio);
            }
            else
            {
              Console.WriteLine("Dinero insuficiente");
            }
          }
          if (m==2)
          {
            if (precio>=25)
            {
              Console.WriteLine("Has Comprado una Coca-Cola sin azucar");
              precio-=25;
              colasina+=1;
              Console.WriteLine("Te quedan RD$ {0}" ,precio);
            }
            else
            {
              Console.WriteLine("Dinero insuficiente");
            }
          }
          if (m==3)
          {
            if (precio>=25)
            {
              Console.WriteLine("Has Comprado una Pepsi");
              precio-=25;
              pepsi+=1;
              Console.WriteLine("Te quedan RD$ {0}" ,precio);
            }
            else
            {
              Console.WriteLine("Dinero insuficiente");
            }
          }
          if (m==4)
          {
            if (precio>=10)
            {
              Console.WriteLine("Has Comprado Kola Real");
              precio-=10;
              kola+=1;
              Console.WriteLine("Te quedan RD$ {0}" ,precio);
            }
            else
            {
              Console.WriteLine("Dinero insuficiente");
            }
          }
          if (m==5)
          {
            if(precio>=15)
            {
              Console.WriteLine("Has Comprado un Santal Melocoton");
              precio-=15;
              melocoton+=1;
              Console.WriteLine("Te quedan RD$ {0}" ,precio);
            }
            else
            {
              Console.WriteLine("Dinero insuficiente");
            }
            
          }
          if (m==6)
          {
            if (precio>=15)
            {
              Console.WriteLine("Has Comprado un Santal Pera");
              precio-=15;
              pera+=1;
              Console.WriteLine("Te quedan RD$ {0}" ,precio);
            }
            else
            {
              Console.WriteLine("Dinero insuficiente");
            }
            
          }
          if (m==7)
          {
            if (precio>=15)
            {
              Console.WriteLine("Has Comprado un Santal Frutpunch");
              precio-=15;
              frutpunch+=1;
              Console.WriteLine("Te quedan RD$ {0}" ,precio);
            }
            else
            {
              Console.WriteLine("Dinero insuficiente");
            }
            
          }
          if (m==8)
          {
            if (precio>=25)
            {
              Console.WriteLine("Has Comprado una Malta India");
              precio-=25;
              india+=1;
              Console.WriteLine("Te quedan RD$ {0}" ,precio);
            }
            else
            {
              Console.WriteLine("Dinero insuficiente");
            }
            
          }
          if (m==9)
          {
            if (precio>=25)
            {
              Console.WriteLine("Has Comprado una Malta Morena");
              precio-=25;
              morena+=1;
              Console.WriteLine("Te quedan RD$ {0}" ,precio);
            }
            else
            {
              Console.WriteLine("Dinero insuficiente");
            }
            
          }
          if (m==10)
          {
            if (precio>=10)
            {
              Console.WriteLine("Has Comprado un Cafe");
              precio-=10;
              cafe+=1;
              Console.WriteLine("Te quedan RD$ {0}" ,precio);
            }
            else
            {
              Console.WriteLine("Dinero insuficiente");
            }
          }
          if (m>=11 || m<=0)
          {
            Console.WriteLine("Invalido");
          }
        }
        //
        
      }while (op!=3);
    }
    public void info()
    {
      Console.WriteLine("**Productos Vendidos**");
      Console.WriteLine("1- Coca-colas vendidas: " +cola);
      Console.WriteLine("2- Coca-colas sin azucar vendidas: " +colasina);
      Console.WriteLine("3- Pepsi vendidas: " +pepsi);
      Console.WriteLine("4- Kola Real vendidas: " +kola);
      Console.WriteLine("5- Santal Melocoton vendidos: " +melocoton);
      Console.WriteLine("6- Santal Pera vendidos: " +pera);
      Console.WriteLine("7- Santal Frutpunch vendidos: " +frutpunch);
      Console.WriteLine("8- Malta india vendidos: " +india);
      Console.WriteLine("9- Malta morena vendidos: " +morena);
      Console.WriteLine("10- Cafe vendidos: " +cafe); 
      Console.ReadKey();
    }
  }
  
  public static void Main (string[] args) 
  {
    Producto pr= new Producto();
    pr.Mostrar();
    pr.info();
  }
}
